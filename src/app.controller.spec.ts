import { getModelToken, MongooseModule } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { Todo } from './todos/entities/todo.entity';
import { TodosModule } from './todos/todos.module';
import { UsersModule } from './users/users.module';

describe('AppController', () => {
  let appController: AppController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [AppService],
      imports: [TodosModule, UsersModule, AuthModule],
    }).compile();

    appController = app.get<AppController>(AppController);
  });

  describe('root', () => {
    xit('should be defined"', () => {
      expect(appController).toBeDefined();
    });
  });
});
