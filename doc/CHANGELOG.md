# CHANGELOG

# [1.3.0](https://gitlab.com/ndiewald/todo-lists-nest/compare/1.2.0...1.3.0) (2022-11-20)


### Bug Fixes

* rename project to todo-lists-nest ([4bbd597](https://gitlab.com/ndiewald/todo-lists-nest/commit/4bbd5972d70b64fd9111e24276816fd2258d0aff))


### Features

* authentication ([4a6173d](https://gitlab.com/ndiewald/todo-lists-nest/commit/4a6173dc9d74fa7c63d0ce452fe4d5e287bd270d))
* **auth:** passport local strategy ([316ca9a](https://gitlab.com/ndiewald/todo-lists-nest/commit/316ca9ab91db08754cb772844e8aaa0dc2f21962))
* **database:** mongodb connection ([88c010d](https://gitlab.com/ndiewald/todo-lists-nest/commit/88c010d5fc4a687d29c1939da67df05c05ad4c96))
* **todos:** CRUD operations ([5d38a4f](https://gitlab.com/ndiewald/todo-lists-nest/commit/5d38a4feb7d3cf133a27676538c16a37936afad7))
* **users:** CRUD operations ([6636600](https://gitlab.com/ndiewald/todo-lists-nest/commit/6636600e8b23c9115cfdf33412dbfd62ce5c62bc))

# [1.2.0](https://gitlab.com/ndiewald/todo-lists-nest/compare/1.1.0...1.2.0) (2021-12-20)


### Features

* helmet ([8a50e28](https://gitlab.com/ndiewald/todo-lists-nest/commit/8a50e28a0ab753ec8667e8c52a2663dd50c6daa5))

# [1.1.0](https://gitlab.com/ndiewald/todo-lists-nest/compare/1.0.0...1.1.0) (2021-12-17)


### Features

* add swagger documentation ([fe20c17](https://gitlab.com/ndiewald/todo-lists-nest/commit/fe20c173820cca5f9207983494e9f8160cd9b56a))
